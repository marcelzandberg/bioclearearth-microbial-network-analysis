#!/usr/bin/env python3

import sys
import argparse
import os.path
from os import path
import os
import PySimpleGUI as sg

__author__ = "Marcel Zandberg"
__status__ = "picrust2 initiator"
__version__ = "2020.16.9.v1"


def get_dir():
    """
    File browser gui to get the directory
    which contains the results.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your data folder')],
              [sg.Text('Folder', size=(8, 1)), sg.Input(), sg.FolderBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def get_count_file():
    """
    File browser gui to get the file
    which contains count data of asv's.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your count file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('File browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def get_asv_file():
    """
    File browser gui to get the file
    which contains taxonomy sequences.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your asv file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('File browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(asv_file):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(asv_file):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    if not asv_file.endswith(".fa"):
        print("error: The file format should be .fa")
        sys.exit(1)
    # return 0 if all good
    return 0


def run_picrust2(env, asv_file):
    try:
        print("Initializing picrust2")
        res = os.popen('sh ./scripts/picrust2.sh ' + str(env) + ' ' + str(asv_file))
        res.read()
    except:
        print("Oops something went wrong")
    print("Your files are ready")
    return 0


def run_biom_converter(env, count_file):
    try:
        print("Converting count table to biom format")
        res = os.popen('sh ./scripts/biom_converter.sh ' + str(env)+' '+str(count_file))
        res.read()
    except:
        print("Oops something went wrong")

    print("Conversion completed")
    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    # count to biom conversion
    env = get_dir()
    count_file = get_count_file()
    run_biom_converter(env, count_file)
    # run picrust2
    asv_file = get_asv_file()
    check_file(asv_file)
    run_picrust2(env, asv_file)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
