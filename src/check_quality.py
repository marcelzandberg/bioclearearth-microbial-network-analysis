#!/usr/bin/env python3

import sys
import argparse
import os.path
from os import path
import os
import PySimpleGUI as sg
import multiqc
import webbrowser
import time


__author__ = "Marcel Zandberg"
__status__ = "FastQC utility"
__version__ = "2020.16.9.v1"


def get_dir():
    """
    File browser gui to get the directory
    which contains the fastq file(s).
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your data folder')],
              [sg.Text('Folder', size=(8, 1)), sg.Input(), sg.FolderBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(fastq_dir):
    """
    Checks if the files exists and checks if it has the right file format.
    """
    # Checks if the path is a directory
    if not path.exists(fastq_dir):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    for files in os.listdir(fastq_dir):
        # checks if file is fastq format
        if not files.endswith(".fastq") or files.endswith(".fq"):
            print("error: The file format should be .fastq")
            sys.exit(1)
    # return 0 if all good
    return 0


def delete_dir():
    """
    Cleans the fastqc folder before every new run.
    """
    # Force delete command
    delete = os.popen('rm -r /media/sf_BioClearEarth/bioclearearth-microbial-network-analysis/fastqc/*')
    print(delete.read())
    return 0


def check_quality(fastq_dir):
    """
    Checks the quality of the forward and reverse strand
    and then summarizes it.
    """
    # run fastqc on fastq files
    fastqc = os.popen('fastqc '+str(fastq_dir)+'/* -o ../fastqc/')
    print(fastqc.read())
    fastqc.close()

    # run multiqc on qc files
    multiqc = os.popen('multiqc ../fastqc/ -o ../fastqc/')
    print(multiqc.read())
    multiqc.close()
    open_report()
    return 0


def open_report():
    """
    checks if the report exists, if exists it will open in the web browser.
    The user gets the choice to change settings uppon analysing the results
    """
    # Path to report file
    outdir = '/media/sf_BioClearEarth/bioclearearth-microbial-network-analysis/fastqc/multiqc_report.html'
    # look for report file
    while not path.exists(outdir):
        print(path.exists(outdir))
        time.sleep(1)

    if path.isfile(outdir):
        # open webbrowser with report
        print("Found report: Opening web browser")
        webbrowser.open('file://'+str(outdir))
        delete = os.popen('rm -r /media/sf_BioClearEarth/bioclearearth-microbial-network-analysis/fastqc/S*')
        print(delete.read())
        sys.exit(0)


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    fastq_dir = get_dir()
    check_file(fastq_dir)
    delete_dir()
    check_quality(fastq_dir)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
