#!/usr/bin/env python3

import sys
import argparse
import os.path
from os import path
import os
import PySimpleGUI as sg
import re


__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"

"""
This program gets the chosen asv sequences out of the seqs file
"""


def get_dir():
    """
    File browser gui to get the directory
    which contains the fastq file(s).
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your data folder')],
              [sg.Text('Folder', size=(8, 1)), sg.Input(), sg.FolderBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(fastq_dir):
    """
    Checks if the files exists and checks if it has the right file format.
    """
    # Checks if the path is a directory
    if not path.exists(fastq_dir):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    for files in os.listdir(fastq_dir):
        # checks if file is fastq format
        if not files.endswith(".fastq") or files.endswith(".fq"):
            print("error: The file format should be .fastq")
            sys.exit(1)
    # return 0 if all good
    return 0


def get_size():
    """
    Settings gui to set the settings for the pipeline.
    """
    # user input settings
    sg.theme('Light Blue 2')
    layout = [[sg.Text('Settings')],
              [sg.Text('Sample name size', size=(16, 1)), sg.InputText()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Settings', layout)

    while True:
        event, values = window.read()
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)

        if event == 'Submit':
            window.close()

            return values[0]


def rename_file(fastq_dir, len_cut):
    print("Using directory: ", fastq_dir)
    len_cut = int(len_cut)
    for file in os.listdir(fastq_dir):
        sample_index = file.find("S")
        new_file_name = file[sample_index:int(len_cut+sample_index)] + "_" + file[:sample_index] + file[sample_index + len_cut + 1:]
        print("Renaming ", file, " > ", new_file_name)
        os.rename(fastq_dir + "/" + file, fastq_dir + "/" + new_file_name)

    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    data = get_dir()
    check_file(data)
    sample_name_size = get_size()
    rename_file(data, sample_name_size)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
