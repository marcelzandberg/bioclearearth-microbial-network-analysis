#!/bin/sh

vsearch -sintax ../usearch/$1/$1.fa -db $2 -strand both -tabbedout ../usearch/$1/$1_tax_raw.txt -sintax_cutoff 0.9
