#!/bin/sh

rm ../usearch/ASV/*

usearch -fastq_mergepairs $1/*_R1_*.fastq -fastq_minmergelen 400 -relabel @ -fastq_pctid $3 -fastq_maxdiffs $2 -fastqout ../usearch/ASV/paired.fastq

usearch -fastx_info ../usearch/ASV/paired.fastq -output ../usearch/ASV/reads_info.txt

usearch -fastx_truncate ../usearch/ASV/paired.fastq -stripleft $4 -stripright $5 -fastqout ../usearch/ASV/primer_trimmed.fq

usearch -fastq_filter ../usearch/ASV/primer_trimmed.fq -fastq_trunclen $7 -fastq_maxee $6 -fastaout ../usearch/ASV/seqs_filtered.fa

usearch -fastx_uniques ../usearch/ASV/seqs_filtered.fa -fastaout ../usearch/ASV/dereplicated_seqs.fasta -sizeout

usearch -unoise3 ../usearch/ASV/dereplicated_seqs.fasta -zotus ../usearch/ASV/ASV.fa

usearch -otutab ../usearch/ASV/trimmed.fq -zotus ../usearch/ASV/ASV.fa -otutabout ../usearch/ASV/ASV_counts.txt -mapout ../usearch/ASV/zmap.txt -biomout ../usearch/ASV/ASV_BIOM.biom
