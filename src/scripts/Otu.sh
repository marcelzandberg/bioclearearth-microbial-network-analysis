#!/bin/sh

rm ../usearch/Otu/*

usearch -fastq_mergepairs $1/*_R1_*.fastq -fastq_minmergelen 400 -fastq_pctid $3 -relabel @ -fastqout ../usearch/Otu/paired.fq -fastq_maxdiffs $2

usearch -fastx_info ../usearch/Otu/paired.fq -output ../usearch/Otu/reads_info.txt

usearch -fastx_truncate ../usearch/Otu/paired.fq -stripleft $4 -stripright $5 -fastqout ../usearch/Otu/primer_trimmed.fq

usearch -fastq_filter ../usearch/Otu/primer_trimmed.fq -fastq_trunclen $7 -fastq_maxee $6 -fastaout ../usearch/Otu/seqs_filtered.fa

usearch -fastx_uniques ../usearch/Otu/seqs_filtered.fa -fastaout ../usearch/Otu/dereplicated_seqs.fa -sizeout

usearch -cluster_otus ../usearch/Otu/dereplicated_seqs.fa -otus ../usearch/Otu/Otu.fa -relabel Otu

usearch -otutab ../usearch/Otu/primer_trimmed.fq -otus ../usearch/Otu/Otu.fa -otutabout ../usearch/Otu/Otu_counts.txt -mapout ../usearch/Otu/map.txt -biomout ../usearch/Otu/Otu_BIOM.biom
