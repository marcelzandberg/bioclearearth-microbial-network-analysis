#!/bin/sh

usearch -fastx_info $1/*_R1_*.fastq -output ../usearch/ASV_unmerged/reads_info.txt

usearch -fastx_truncate $1/*_R1_*.fastq -stripleft $2 -fastqout ../usearch/ASV_unmerged/primer_trimmed.fq

usearch -fastq_filter ../usearch/ASV_unmerged/primer_trimmed.fq -fastq_trunclen $4 -fastq_maxee $3 -fastaout ../usearch/ASV_unmerged/seqs_filtered.fa

usearch -fastx_uniques ../usearch/ASV_unmerged/seqs_filtered.fa -fastaout ../usearch/ASV_unmerged/dereplicated_seqs.fasta -sizeout

usearch -unoise3 ../usearch/ASV_unmerged/dereplicated_seqs.fasta -zotus ../usearch/ASV_unmerged/ASV_unmerged.fa 

usearch -otutab ../usearch/ASV_unmerged/primer_trimmed.fq -zotus ../usearch/ASV_unmerged/ASV_unmerged.fa -otutabout ../usearch/ASV_unmerged/ASV_counts.txt -mapout ../usearch/ASV_unmerged/zmap.txt -biomout ../usearch/ASV_unmerged/ASV_BIOM.biom


