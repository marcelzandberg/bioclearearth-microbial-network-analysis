#!/bin/sh


mkdir $1/raw
mkdir $1/truncated

for fq in $1/*_R2_*.fastq
do
   usearch -fastx_truncate $fq -trunclen $2 -padlen $2 -fastqout $fq.truncated.fastq
   mv $fq $1/raw/
   mv $fq.truncated.fastq $fq
done

cp $1/*_R1_*.fastq $1/raw/
mv $1/*.fastq $1/truncated
