#!/bin/sh

for fq in $1/*_R1_*.fastq
do
   usearch -fastx_truncate $fq -stripleft $2 -fastqout $fq.truncated.fq
   INPUT=$fq
   SUBSTRING=$(echo $INPUT| cut -d'S' -f 2)
   SAMPLE=$(echo $SUBSTRING| cut -d'_' -f 1)
   echo $SAMPLE
   usearch -fastx_relabel $fq.truncated.fq -prefix S$SAMPLE. -fastqout $fq.labeled.fq -keep_annots
done

for f in $1/*_R1_*.fastq.labeled.fq
do
   usearch -fastq_filter $f -fastq_maxee $3 -fastq_trunclen $4 -relabel @ -fastaout $f.filtered.fq
done

cat $1/*.filtered.fq > ../usearch/Otu_unmerged/filtered.fa
cat $1/*.labeled.fq > ../usearch/Otu_unmerged/primer_trimmed.fq

rm -f $1/*.truncated.*
rm -f $1/*.labeled.*

usearch -fastx_uniques ../usearch/Otu_unmerged/filtered.fa -fastaout ../usearch/Otu_unmerged/dereplicated_seqs.fasta -sizeout

usearch -cluster_otus ../usearch/Otu_unmerged/dereplicated_seqs.fasta -otus ../usearch/Otu_unmerged/Otu_unmerged.fa -relabel Otu

usearch -otutab ../usearch/Otu_unmerged/primer_trimmed.fq -otus ../usearch/Otu_unmerged/Otu_unmerged.fa -otutabout ../usearch/Otu_unmerged/Otu_counts.txt -mapout ../usearch/Otu_unmerged/map.txt -biomout ../usearch/Otu_unmerged/Otu_BIOM.biom
