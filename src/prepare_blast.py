#!/usr/bin/env python3

import sys
from os import path
import PySimpleGUI as sg


__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"
"""
This program parses the chosen data to a format that is ready to blast
"""

def get_file():
    """
    File browser gui to get the file
    which contains taxonomy sequences.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your output sig seq file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(asv_file):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(asv_file):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    if not asv_file.endswith(".fa"):
        print("error: The file format should be .fa")
        sys.exit(1)
    # return 0 if all good
    return 0


def get_amplicons(asv_file):
    """
    Parses the retrieved data to fa format
    """
    with open(asv_file) as file:
        str = ""
        for line in file:
            if not line.startswith('"V2"'):
                # split zotus en seq
                element = line.split(" ")
                # creates new line with > header for fa format
                newline = ">"+element[0]+"\n"+element[1]
                # strip trailing quotes
                str += newline.replace('"', '')

    text_file = open(asv_file, "w")
    # write file
    text_file.write(str)
    print("File processing done")
    # close file
    text_file.close()
    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    asv_file = get_file()
    check_file(asv_file)
    get_amplicons(asv_file)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
