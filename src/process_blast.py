#!/usr/bin/env python3

import sys
from os import path
import PySimpleGUI as sg
import itertools

__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"
"""
This program parses BLAST data to a format that is human readable
"""

def get_file():
    """
    File browser gui to get the file
    which contains taxonomy sequences.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your Blast result file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(blast_file):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(blast_file):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    if not blast_file.endswith(".txt"):
        print("error: The file format should be .txt")
        sys.exit(1)
    # return 0 if all good
    return 0


def get_results(blast_file):
    """
    Parses the retrieved data to fa format
    """

    with open(blast_file) as file:
        str = ""
        for line in file:
            # parse from..
            if line.startswith("Query #"):
                str += line
            if line.startswith("Sequences producing significant alignments"):
                for i in itertools.islice(file, 12):
                    str += i
                str += "\n"

    text_file = open(blast_file, "w")
    # write file
    text_file.write(str)
    print("File processing done")
    # close file
    text_file.close()
    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    blast_file = get_file()
    check_file(blast_file)
    get_results(blast_file)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
