#!/usr/bin/env python3

import sys
import os.path
from os import path
import os
import PySimpleGUI as sg


__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"

"""
This program contains and initiates a bioinformatic pipeline
"""


def select_program():
    """
    Select the program that the user wants to use
    """
    sg.theme('Light Blue 2')
    # Program selection UI
    layout = [[sg.Text('What program do you want to choose?')],
              [sg.Text('ASV pipeline', size=(16, 1)), sg.Button('script 1')],
              [sg.Text('Otu pipeline', size=(16, 1)), sg.Button('script 2')],
              [sg.Text('Discard ASV R2', size=(16, 1)), sg.Button('script 3')],
              [sg.Text('Discard Otu R2', size=(16, 1)), sg.Button('script 4')],
              [sg.Button('Exit')]]

    window = sg.Window('Program selection', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'script 1':
            window.close()
            # return script value for ASV pipeline
            return 'ASV'
        elif event == 'script 2':
            window.close()
            # return script value for Otu pipeline
            return 'Otu'
        elif event == 'script 3':
            window.close()
            # return script value asv without r2
            return 'ASV_unmerged'
        elif event == 'script 4':
            window.close()
            # return script value otu without r2
            return 'Otu_unmerged'


def get_dir():
    """
    File browser gui to get the directory
    which contains the fastq file(s).
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your data folder')],
              [sg.Text('Folder', size=(8, 1)), sg.Input(), sg.FolderBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def get_file():
    """
    File browser gui to get the file
    which contains taxonomy sequences.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your db file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('File browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def set_settings():
    """
    Settings gui to set the settings for the pipeline.
    """
    # user input settings
    sg.theme('Light Blue 2')
    layout = [[sg.Text('Settings')],
              [sg.Text('Max differences(bp)', size=(20, 1)), sg.InputText()],
              [sg.Text('Percentage Identity', size=(20, 1)), sg.InputText()],
              [sg.Text('R1 primer length(bp)', size=(20, 1)), sg.InputText()],
              [sg.Text('R2 primer length(bp)', size=(20, 1)), sg.InputText()],
              [sg.Text('Quality threshold(1.0)', size=(20, 1)), sg.InputText()],
              [sg.Text('Truncate sequence(bp)', size=(20, 1)), sg.InputText()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Settings', layout)

    while True:
        event, values = window.read()
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)

        if event == 'Submit':
            window.close()
            # run usearch pipeline

            return values[0], values[1], values[2], values[3], values[4], values[5]


def check_file(fastq_dir):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(fastq_dir):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    for files in os.listdir(fastq_dir):
        # checks if file is fastq format
        if not files.endswith(".fastq"):
            print("error: The file format should be .fastq")
            sys.exit(1)
    # return 0 if all good
    return 0


def run_usearch(fastq_dir, max_diffs, min_pctid, r1_primer_length, r2_primer_length, quality_threshold, truncate_sequences, program):
    """
    Runs usearch
    """
    # runs the usearch script with given parameters
    if program == 'ASV':
        print("using ASV.sh")
        res = os.popen('sh ./scripts/ASV.sh '
                       + str(fastq_dir) + ' ' + str(max_diffs) + ' ' + str(min_pctid) + ' ' + str(r1_primer_length) + ' '
                       + str(r2_primer_length) + ' ' + str(quality_threshold) + ' ' + str(truncate_sequences))
        # read the lines
        print('sh ./scripts/ASV.sh '
              + str(fastq_dir) + ' ' + str(max_diffs) + ' ' + str(min_pctid) + ' ' + str(r1_primer_length) + ' '
              + str(r2_primer_length) + ' ' + str(quality_threshold) + ' ' + str(truncate_sequences))
        res.read()
    # runs the usearch script without r2 read
    elif program == 'Otu':
        print("using Otu.sh")

        res = os.popen('sh ./scripts/Otu.sh '
                       + str(fastq_dir) + ' ' + str(max_diffs) + ' ' + str(min_pctid) + ' ' + str(r1_primer_length) + ' '
                       + str(r2_primer_length) + ' ' + str(quality_threshold) + ' ' + str(truncate_sequences))
        # read the lines
        print('sh ./scripts/Otu.sh '
              + str(fastq_dir) + ' ' + str(max_diffs) + ' ' + str(min_pctid) + ' ' + str(r1_primer_length) + ' '
              + str(r2_primer_length) + ' ' + str(quality_threshold) + ' ' + str(truncate_sequences))
        res.read()
    elif program == 'ASV_unmerged':
        print("using ASV_without_r2.sh")

        res = os.popen('sh ./scripts/ASV_without_r2.sh '
                       + str(fastq_dir) + ' ' + str(r1_primer_length) + ' '
                       + str(quality_threshold) + ' ' + str(truncate_sequences))
        # read the lines
        print('sh ./scripts/ASV_without_r2.sh '
              + str(fastq_dir) + ' ' + str(r1_primer_length) + ' '
              + str(quality_threshold) + ' ' + str(truncate_sequences))
        res.read()
    elif program == 'Otu_unmerged':
        print("using Otu_without_r2.sh")

        res = os.popen('sh ./scripts/Otu_without_r2.sh '
                       + str(fastq_dir) + ' ' + str(r1_primer_length) + ' '
                       + str(quality_threshold) + ' ' + str(truncate_sequences))
        # read the lines
        print('sh ./scripts/Otu_without_r2.sh '
              + str(fastq_dir) + ' ' + str(r1_primer_length) + ' '
              + str(quality_threshold) + ' ' + str(truncate_sequences))
        res.read()
    # change settings
    change_settings(fastq_dir, program)

    return 0


def change_settings(fastq_dir, program):
    """
    Asks the user if he wants to change pipeline settings.
    if prompted yes: then the user can change the settings
    by input. if no: then the program is done.
    """
    sg.popup("Files are ready for classification")
    answer = None
    while answer not in ("yes", "no"):
        answer = sg.PopupYesNo('Do you want a new run and change settings?')
        if answer == "Yes":
            # visualize settings box
            # show different settings for the 2 scripts

            # user input settings
            sg.theme('Light Blue 2')
            layout = [[sg.Text('Settings')],
                      [sg.Text('Max differences(bp)', size=(20, 1)), sg.InputText()],
                      [sg.Text('Percentage Identity', size=(20, 1)), sg.InputText()],
                      [sg.Text('R1 primer length(bp)', size=(20, 1)), sg.InputText()],
                      [sg.Text('R2 primer length(bp)', size=(20, 1)), sg.InputText()],
                      [sg.Text('Quality threshold(1.0)', size=(20, 1)), sg.InputText()],
                      [sg.Text('Truncate sequence(bp)', size=(20, 1)), sg.InputText()],
                      [sg.Button('Submit'), sg.Button('Exit')]]

            window = sg.Window('Settings', layout)
            while True:
                event, values = window.read()
                if event == 'Exit' or event == sg.WIN_CLOSED:
                    sys.exit(0)

                if event == 'Submit':
                    window.close()
                    # run usearch pipeline
                    run_usearch(fastq_dir, values[0], values[1], values[2], values[3], values[4], values[5], program)

        elif answer == "No":
            print("\nFiles are ready for classification")

        else:
            print("Please enter Yes or No.")
        # call method
        classify_sequences(program)


def classify_sequences(program):
    """
    Asks the user if he wants to do taxonomy analysis
    if yes: script will run.
    """
    answer = None
    while answer not in ("yes", "no"):
        answer = sg.PopupYesNo('Do you want to do taxonomy analysis?\nThis process is going to take a while')
        if answer == "Yes":
            db_file = get_file()
            res = os.popen('sh ./scripts/taxonomy.sh ' + str(program) + ' ' + str(db_file))
            res.read()
            print("\nFiles are ready for classification")
            sys.exit(0)
        elif answer == "No":
            print("\nFiles are ready for classification")
            sys.exit(0)
        else:
            print("Please enter Yes or No.")
    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    program = select_program()
    fastq_dir = get_dir()
    max_diffs, min_pctid, r1_primer_length, r2_primer_length, quality_threshold, truncate_sequences = set_settings()
    check_file(fastq_dir)
    run_usearch(fastq_dir, max_diffs, min_pctid, r1_primer_length, r2_primer_length, quality_threshold, truncate_sequences, program)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
