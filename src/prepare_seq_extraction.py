#!/usr/bin/env python3

import sys
import argparse
import os.path
from os import path
import os
import PySimpleGUI as sg
import re


__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"
"""
This program gets the chosen asv sequences out of the seqs file
"""

def get_file():
    """
    File browser gui to get the file
    which contains taxonomy sequences.
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your seq file')],
              [sg.Text('File', size=(8, 1)), sg.Input(), sg.FileBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(asv_file):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(asv_file):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    if not asv_file.endswith(".fa"):
        print("error: The file format should be .fa")
        sys.exit(1)
    # return 0 if all good
    return 0


def get_amplicons(asv_file):

    text_file = open("../"
                     "", "w")
    with open(asv_file) as file:
        str = ""
        for line in file:
            if line.startswith(">"):
                element = line.strip("\n")
                element2 = element.strip(">")
                newline = "\n"+element2+","
                str += newline
            else:
                element = line.strip("\n")
                str += element
    text_file.write(str)
    print("File processing done")
    text_file.close()
    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    asv_file = get_file()
    check_file(asv_file)
    get_amplicons(asv_file)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
