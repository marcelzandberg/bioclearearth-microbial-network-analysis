#!/usr/bin/env python3

import sys
import os.path
from os import path
import os
import PySimpleGUI as sg


__author__ = "Marcel Zandberg"
__status__ = "ASV creator"
__version__ = "2020.16.9.v1"

"""
This program contains and initiates a bioinformatic pipeline
"""


def get_dir():
    """
    File browser gui to get the directory
    which contains the fastq file(s).
    """
    sg.theme('Light Blue 2')
    # File browser UI
    layout = [[sg.Text('Choose your data folder')],
              [sg.Text('Folder', size=(8, 1)), sg.Input(), sg.FolderBrowse()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Directory browser', layout)

    while True:
        event, value = window.read()
        # Exit system uppon user input
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)
        if event == 'Submit':
            window.close()
            # Has the directory path
            return value[0]


def check_file(fastq_dir):
    """
    Checks if the files exists and checks if it has the right file format
    """
    # Checks if the path is a directory
    if not path.exists(fastq_dir):
        print("error: The path to the file does not exist")
        sys.exit(1)
    # Loops through all the files in the directory
    for files in os.listdir(fastq_dir):
        # checks if file is fastq format
        if not files.endswith(".fastq"):
            print("error: The file format should be .fastq")
            sys.exit(1)
    # return 0 if all good
    return 0


def set_settings():
    """
    Settings gui to set the settings for the pipeline.
    """
    # user input settings
    sg.theme('Light Blue 2')
    layout = [[sg.Text('Settings')],
              [sg.Text('Truncate', size=(12, 1)), sg.InputText()],
              [sg.Button('Submit'), sg.Button('Exit')]]

    window = sg.Window('Settings', layout)

    while True:
        event, values = window.read()
        if event == 'Exit' or event == sg.WIN_CLOSED:
            sys.exit(0)

        if event == 'Submit':
            window.close()
            # run usearch pipeline

            return values[0]


def run_usearch(fastq_dir, truncate):
    """
    Runs usearch
    """
    # runs the usearch script with given parameters

    print("using truncate_r2.sh")
    res = os.popen('sh ./scripts/truncate_r2.sh '
                   + str(fastq_dir) + ' ' + str(truncate))
    # read the lines
    print('sh ./scripts/truncate_r2.sh '
                   + str(fastq_dir) + ' ' + str(truncate))
    res.read()

    return 0


def main(args):
    """
    The main of the program.
    Controlling the flow.
    """
    fastq_dir = get_dir()
    check_file(fastq_dir)
    truncate = set_settings()
    run_usearch(fastq_dir, truncate)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
