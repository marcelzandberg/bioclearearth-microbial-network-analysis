![poster](./Docs/Emblems/BioClearEarth.png "Poster")

# Introduction to Microbial Network Analysis

This repository contains all code to identify key species in the microbial community of farmland

It is based on a usearch pipeline which classifies 16s rna of soil samples

All code is kept up-to-date in this Git repository


## Getting started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

Things you need to install to get everything up and running:

* For windows users:
    * Install virtual box: https://www.virtualbox.org/wiki/Downloads
        * create virtual enviroment to use linux/ubuntu
* From the linux enviroment in the terminal:
    * pip install multiqc
    * pip install pysimplegui
    * python --version (needs to be 3.6.7)
        * sudo apt-get update
        * sudo apt-get install python3.6.7
    
    
#### Program installation

* Download FastQC: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
    * Make executable: chmod 755 fastqc
    * Set path variable: sudo ln -s /path/to/FastQC/fastqc /usr/local/bin/fastqc

* Download Usearch: https://drive5.com/usearch/download.html 
    * gunzip usearchx_i86linux32.gz
    * Set path variable: mv usr/local/bin usearch
    * Make executable: chmod +x usearch

## Setup the project

clone the repo in the desired folder
* git clone..

To use the pipeline: Go to the src folder of the project and open a terminal

* Check quality of reads: python3 check_quality.py

* Create ASV's: python3 unoise.py
    * With optional arguments: python3 unoise.py
        * -d set max diffs for merge pairs
        * -r strip nucleotides of read right side
        * -l strip nucleotides of read left side
        * -t sequences are truncated at length n

* Get sequences of desired asvs: ampliconfinder.py
    * I asvs.fa
    * O desired_asvs.fa

* Desired sequences to blast: prepare_blast.py
    * I desired_asvs.fa
    * O blast.fa

## To do the analysis

* Open file 'network_analysis.Rmd' in the src folder with RStudio
    * The file contains the code to the standard microbiome analysis

* Open file 'network_creation.Rmd' in the src folder with RStudio
    * The file contains the code to create microbiome networks
    * Optionally use 'network_creation_normalized.Rmd' for normalized data
    
* Run 'create_edgelist.py' to get  all the edges of connected notes in the network
    * I edgelist_raw.txt
    * O edgelist.txt
    

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on my code of conduct, and the process for submitting pull requests to me.


## Versioning

I will use [semver](http://semver.org/) for versioning. But since this is the initial release of this software, there aren't any versions available yet.


## Author

* **Marcel Zandberg** - *Initial work* - [Portfolio](https://bioinf.nl/~mazandberg) | [Bitbucket](https://bitbucket.org/marcelzandberg)


## License

* Not available yet
